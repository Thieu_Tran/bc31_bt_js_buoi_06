// Bài 1: Tìm số nguyên dương nhỏ nhất
document.getElementById("btn-bai1").addEventListener("click",function(){
    sum = 0;
    
    for(var count = 1; sum<10000; count++){
        sum+= count;
    }

    // Khi điều kiện (sum <10000) không thỏa thì biến count vẫn còn tăng lên +1, do đó giá trị count mà tại đó sum vừa bắt đầu lớn hơn 10000 là count-1
    document.getElementById("kq1").innerHTML = `Số nguyên dương nhỏ nhất: ${count-1}`;
})

// Bài 2: Tính tổng

document.getElementById("btn-bai2").addEventListener("click",function(){
    var xValue = document.getElementById("txt-numx").value*1;
    var nValue = document.getElementById("txt-numn").value*1;

    var sum = 0;
    for(var n = 1; n <= nValue; n++){
        sum+= Math.pow(xValue,n)
    }
    document.getElementById("kq2").innerHTML = `Tổng: ${sum}`;
})

// Bài 3: Tính giai thừa
document.getElementById("btn-bai3").addEventListener("click",function(){
    var numGiaiThuaValue = document.getElementById("num-b3").value*1;

    var giaiThua = 1;

    if(numGiaiThuaValue < 0){
        document.getElementById("kq3").innerHTML = "Mời nhập số nguyên dương"
    }
    else if(numGiaiThuaValue == 0){
        giaiThua = 1;
    document.getElementById("kq3").innerHTML = `${numGiaiThuaValue}! = ${giaiThua}`;

    }
    else{
        for(var i = 1;i <= numGiaiThuaValue; i++){
            giaiThua = giaiThua*i; 
        }
        document.getElementById("kq3").innerHTML = `${numGiaiThuaValue}! = ${giaiThua}`;
    }
})

// Bài 4: Tạo thẻ div
document.getElementById("btn-bai4").addEventListener("click",function(){
    var contentLe = `<div style="background:blue;color:white"> Div lẻ </div>`;
    var contentChan = `<div style="background:red;color:white"> Div chẵn </div>`;

    var contentHTML = "";
    for(var count = 1; count <= 10; count++){
        if(count%2 == 0){
            contentHTML += contentChan;
        }
        else{
            contentHTML += contentLe;
        }
    }
    document.getElementById("kq4").innerHTML = contentHTML;
})